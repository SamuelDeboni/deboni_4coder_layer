#### Build instructions
- Clone in the 4coder folder
```
./custom/bin/buildsuper_x64-linux.sh deboni_4coder_layer/deboni_layer.cpp
```

## Default Keybindings
### Shared Bindings
    
- go_to_normal_mode => Escape
- move_left =>         Left
- move_right =>        Right
- move_up =>           Up
- move_down =>         Down
    
- move_left_alpha_numeric_boundary =>  Left,  Control
- move_right_alpha_numeric_boundary => Right, Control
- move_up_to_blank_line   =>  Up, Control
- move_down_to_blank_line =>  Down,  Control
- move_line_up =>    Up,   Shift
- move_line_down =>  Down, Shift
    
- backspace_char =>                   Backspace
- delete_char =>                      Delete
- backspace_alpha_numeric_boundary => Backspace, Control
    
### Normal Mode bindings
    
#### Navigation
- move_left_alpha_numeric_boundary =>  H => A
- move_right_alpha_numeric_boundary => L, A
- move_up_to_blank_line =>    K, A
- move_down_to_blank_line =>  J, A
	
- move_line_up =>    K, Shift
- move_line_down =>  J, Shift
    
- move_left_alpha_numeric_boundary =>  H, S
- move_right_alpha_numeric_boundary => L, S
- move_up_to_blank_line =>    K, S
- move_down_to_blank_line =>  J, S
    
- move_left  => H
- move_right => L
- move_up    => K
- move_down  => J
    
- go_to_insert_mode => I
- set_mark          => Space
- cursor_mark_swap  => Space, Shift
- set_mark          => S
    
- insert_at_end       => I, Shift
- insert_at_beginning => I, Alt
- insert_on_bottom    => O
- insert_on_top       => O, Shift

- goto_line              => G
- goto_beginning_of_line => G, Shift
- goto_end_of_line       => G, Control

#### Character manipulation
- delete_char  => C, Shift
- cut          => C
- delete_line  => D, Shift
- delete_range => D
    
- select_line    => X
- duplicate_line => X, Shift

- comment_line_toggle => Z
    
- undo => U
- redo => U, Shift
    
- copy  => Y
- paste => P
    
- modal_write_todo => T
- modal_write_note => T, Shift
- modal_write_hack => T, Alt
    
- save => S, Control
- new_line_insert => Return
    
#### Search and replace
- search => F
- reverse_search => F, Shift
- query_replace => R
- query_replace_selection => R, Shift
    
#### Open file and panels
- interactive_open_or_new => N
- change_active_panel => W
- open_panel_vsplit => W, Shift
- open_panel_hsplit => W, Alt
- close_panel => W, Control
- interactive_kill_buffer => B, Shift
- interactive_switch_buffer => B
- kill_buffer => B, Control
    
#### Macros
- start_macro_recording => Q, Shift
- end_macro_recording => Q
- keyboard_macro_replay => M
    
- command_lister => Semicolon, Shift

#### Insert mode bindings
- insert_tab => Tab
