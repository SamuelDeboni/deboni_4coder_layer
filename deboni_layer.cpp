#if !defined(DEBONI_BINDINGS_CPP)
#define DEBONI_BINDINGS_CPP

#include "../custom/4coder_default_include.cpp"


CUSTOM_ID(command_map, mapid_shared);
CUSTOM_ID(command_map, mapid_normal);
CUSTOM_ID(command_map, mapid_insert);

static void
set_current_mapid(Application_Links *app, Command_Map_ID mapid)
{
	View_ID view = get_active_view(app, 0);
	Buffer_ID buffer = view_get_buffer(app, view, 0);
	Managed_Scope scope = buffer_get_managed_scope(app, buffer);
	Command_Map_ID *map_id_ptr = scope_attachment(app, scope, buffer_map_id, Command_Map_ID);
	*map_id_ptr = mapid;
}

#if !defined(META_PASS)
#include "generated/managed_id_metadata.cpp"
#endif

enum CurrentBufferLanguage {
    CurrentBufferLanguage_C = 0,
    CurrentBufferLanguage_Zig,
};

static CurrentBufferLanguage global_current_buffer_language = CurrentBufferLanguage_C;
static bool is_using_zig_fmt = false;

#include "deboni_4coder_custom_render.cpp"

static bool global_is_recording_a_macro = false;

static void
changeColorToInsert() {
	active_color_table.arrays[defcolor_cursor].vals[0] = 0xff80ff80;
	active_color_table.arrays[defcolor_at_cursor].vals[0] = 0xff293143;
    
    if (global_is_recording_a_macro) {
		active_color_table.arrays[defcolor_margin_active].vals[0] = 0xff8080ff;
    } else {
		active_color_table.arrays[defcolor_margin_active].vals[0] = 0xff80ff80;
    }
}

static void
changeColorToNormal() {
	active_color_table.arrays[defcolor_cursor].vals[0] = 0xffff5050;
	active_color_table.arrays[defcolor_at_cursor].vals[0] = 0xff293143;
    
    if (global_is_recording_a_macro) {
		active_color_table.arrays[defcolor_margin_active].vals[0] = 0xff8080ff;
    } else {
		active_color_table.arrays[defcolor_margin_active].vals[0] = 0xffff5050;
    }
}

CUSTOM_COMMAND_SIG(go_to_normal_mode)
CUSTOM_DOC("Change map to normal mode")
{
	set_current_mapid(app, mapid_normal);
    changeColorToNormal();
}

CUSTOM_COMMAND_SIG(go_to_insert_mode)
CUSTOM_DOC("Change map to insert mode")
{
	set_current_mapid(app, mapid_insert);
    changeColorToInsert();
}

CUSTOM_COMMAND_SIG(start_macro_recording)
CUSTOM_DOC("Start macro recording")
{
	keyboard_macro_start_recording(app);
    active_color_table.arrays[defcolor_margin_active].vals[0] = 0xff8080ff;
    global_is_recording_a_macro = true;
}

CUSTOM_COMMAND_SIG(end_macro_recording)
CUSTOM_DOC("Ende macro recording")
{
	keyboard_macro_finish_recording(app);
    global_is_recording_a_macro = false;
    go_to_normal_mode(app);
}

CUSTOM_COMMAND_SIG(modal_write_todo)
CUSTOM_DOC("Write // TODO(NAME): and go to insert mode")
{
	write_todo(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(modal_write_hack)
CUSTOM_DOC("Write // HACK(NAME): and go to insert mode")
{
	write_hack(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(modal_write_note)
CUSTOM_DOC("Write // NOTE(NAME): and go to insert mode")
{
	write_note(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(insert_on_bottom)
CUSTOM_DOC("Insert a line bellow and go to insert mode")
{
	seek_end_of_line(app);
	write_text(app, string_u8_litexpr("\n"));
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(insert_on_top)
CUSTOM_DOC("Insert a line above and go to insert mode")
{
	seek_beginning_of_line(app);
	write_text(app, string_u8_litexpr("\n"));
	move_up(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(insert_at_end)
CUSTOM_DOC("Go to the end of line end change to insert mode")
{
	seek_end_of_line(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(insert_at_beginning)
CUSTOM_DOC("Go to the beginning of the line en change to insert mode")
{
    seek_beginning_of_line(app);
	move_right_token_boundary(app);
	move_left_token_boundary(app);
	go_to_insert_mode(app);
}

CUSTOM_COMMAND_SIG(select_line)
CUSTOM_DOC("Select the line with the cursor on top")
{
	seek_beginning_of_line(app);
	set_mark(app);
	seek_end_of_line(app);
}

CUSTOM_COMMAND_SIG(insert_tab)
CUSTOM_DOC("Insert 4 spaces")
{
    write_text(app, string_u8_litexpr("    "));
}

CUSTOM_COMMAND_SIG(new_line_insert)
CUSTOM_DOC("Insert a new line character and go to insert mode")
{
    write_text_and_auto_indent(app);
    go_to_insert_mode(app);
}


CUSTOM_COMMAND_SIG(custom_backspace)
{
    if (is_using_zig_fmt && global_current_buffer_language == CurrentBufferLanguage_Zig) {
        
        View_ID view = get_active_view(app, 0);
        Buffer_ID buffer = view_get_buffer(app, view, 0);
        
        i64 cursor_pos = view_get_cursor_pos(app, view);
        Range_i64 range = {};
        range.max = cursor_pos;
        range.min = cursor_pos - 4;
        if (range.min < 0) range.min = 0;
        
        u8 out[4];
        
        b32 success = buffer_read_range(app, buffer, range, out);
        if (success && out[3] == ' ') {
            for (int i = 3; i >= 0; i--) {
                if (out[i] != ' ') {
                    range.min += i + 1;
                    break;
                }
            }
            buffer_replace_range(app, buffer, range, string_u8_litexpr(""));
        } else {
            backspace_char(app);
        }
    } else {
        backspace_char(app);
    }
}

static char sbuff[512];

CUSTOM_COMMAND_SIG(zig_fmt) {
    if (is_using_zig_fmt && global_current_buffer_language == CurrentBufferLanguage_Zig) {
		Scratch_Block scratch(app);
        
		View_ID view = get_active_view(app, 0);
		Buffer_ID buffer = view_get_buffer(app, view, 0);
        
		String_Const_u8 buf_name = push_buffer_file_name(app, scratch, buffer);
		sprintf(sbuff, "zig fmt %s", (char *)&buf_name);
        
#ifndef _WIN32
		system(sbuff);
#endif
        
        // TODO(Samuel): make zig_fmt work without using reopen
		buffer_reopen(app, buffer, 0);
		center_view(app);
	}
}

CUSTOM_COMMAND_SIG(custom_save)
CUSTOM_DOC("Insert a new line character and go to insert mode")
{
    clean_all_lines(app);
	save(app);
    zig_fmt(app);
}


static b32
deboni_check_language(Application_Links *app, Buffer_ID buffer_id)
{
    b32 treat_as_code = false;
    Scratch_Block scratch(app);
    
    String_Const_u8 file_name = push_buffer_file_name(app, scratch, buffer_id);
    if (file_name.size > 0){
        String_Const_u8_Array extensions = global_config.code_exts;
        String_Const_u8 ext = string_file_extension(file_name);
        for (i32 i = 0; i < extensions.count; ++i){
            if (string_match(ext, extensions.strings[i])){
                
                if(string_match(ext, string_u8_litexpr("zig")))
                {
                    treat_as_code = true;
                    global_current_buffer_language = CurrentBufferLanguage_Zig;
                    
                    if (is_using_zig_fmt) {
                        global_config.enable_virtual_whitespace = false;
                    }
                }
                else if (string_match(ext, string_u8_litexpr("cpp")) ||
                         string_match(ext, string_u8_litexpr("h")) ||
                         string_match(ext, string_u8_litexpr("odin")) ||
                         string_match(ext, string_u8_litexpr("rs")) ||
                         string_match(ext, string_u8_litexpr("java")) ||
                         string_match(ext, string_u8_litexpr("cs")) ||
                         string_match(ext, string_u8_litexpr("c")) ||
                         string_match(ext, string_u8_litexpr("hpp")) ||
                         string_match(ext, string_u8_litexpr("cc")) ||
                         string_match(ext, string_u8_litexpr("glsl")) ||
                         string_match(ext, string_u8_litexpr("vert")) ||
                         string_match(ext, string_u8_litexpr("frag")) ||
                         string_match(ext, string_u8_litexpr("go")) ||
                         string_match(ext, string_u8_litexpr("kt")) ||
                         string_match(ext, string_u8_litexpr("ino")))
                {
                    treat_as_code = true;
                    global_current_buffer_language = CurrentBufferLanguage_C;
                    
                    if (is_using_zig_fmt) {
                        global_config.enable_virtual_whitespace = true;
                    }
                }
                
                break;
            }
        }
    }
    
    return treat_as_code;
}

CUSTOM_COMMAND_SIG(custom_click_activate_view)
CUSTOM_DOC("Does some stuff when click activate view")
{
    click_set_cursor_and_mark(app);
    
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer_id = view_get_buffer(app, view, Access_Always);
    deboni_check_language(app, buffer_id);
}

CUSTOM_COMMAND_SIG(custom_change_active_panel)
{
	change_active_panel(app);
    
    View_ID view = get_active_view(app, Access_Always);
    Buffer_ID buffer_id = view_get_buffer(app, view, Access_Always);
    deboni_check_language(app, buffer_id);
}

static void
setupBindings(Thread_Context *tctx, Application_Links *app)
{
    
	mapping_init(tctx, &framework_mapping);
	// NOTE(Samuel): uncoment to use default bindings
	// setup_default_mapping(&framework_mapping, mapid_global, mapid_file, mapid_code);
    
	// Custom mapping start
	MappingScope();
	SelectMapping(&framework_mapping);
    
	// Bindings necessary in order to 4coder to work
	SelectMap(mapid_global);
	BindCore(default_startup, CoreCode_Startup);
	BindCore(default_try_exit, CoreCode_TryExit);
	Bind(exit_4coder, KeyCode_F4, KeyCode_Alt);
	BindMouseWheel(mouse_wheel_scroll);
	BindMouseWheel(mouse_wheel_change_face_size, KeyCode_Control);
	BindMouse(click_set_cursor_and_mark        , MouseCode_Left);
    BindMouseRelease(click_set_cursor          , MouseCode_Left);
    BindCore(custom_click_activate_view        , CoreCode_ClickActivateView);
    BindMouseMove(click_set_cursor_if_lbutton);
    
	// ==== My bindings ====
    
	// ==== Shared Bindings ====
	SelectMap(mapid_shared);
	ParentMap(mapid_global);
    
	Bind(go_to_normal_mode, KeyCode_Escape);
	Bind(move_left,         KeyCode_Left);
	Bind(move_right,        KeyCode_Right);
	Bind(move_up,           KeyCode_Up);
	Bind(move_down,         KeyCode_Down);
    
	Bind(move_left_alpha_numeric_boundary,  KeyCode_Left,  KeyCode_Control);
	Bind(move_right_alpha_numeric_boundary, KeyCode_Right, KeyCode_Control);
	Bind(move_up_to_blank_line,            KeyCode_Up,    KeyCode_Control);
	Bind(move_down_to_blank_line,          KeyCode_Down,  KeyCode_Control);
	Bind(move_line_up,    KeyCode_Up,   KeyCode_Shift);
	Bind(move_line_down,  KeyCode_Down, KeyCode_Shift);
    
	Bind(custom_backspace,                   KeyCode_Backspace);
	Bind(delete_char,                      KeyCode_Delete);
	Bind(backspace_alpha_numeric_boundary, KeyCode_Backspace, KeyCode_Control);
    Bind(custom_save, KeyCode_S, KeyCode_Control);
    
    Bind(project_fkey_command, KeyCode_F1);
    Bind(project_fkey_command, KeyCode_F2);
    Bind(project_fkey_command, KeyCode_F3);
    Bind(project_fkey_command, KeyCode_F4);
    Bind(project_fkey_command, KeyCode_F5);
    Bind(project_fkey_command, KeyCode_F6);
    Bind(project_fkey_command, KeyCode_F7);
    Bind(project_fkey_command, KeyCode_F8);
    Bind(project_fkey_command, KeyCode_F9);
    Bind(project_fkey_command, KeyCode_F10);
    Bind(project_fkey_command, KeyCode_F11);
    Bind(project_fkey_command, KeyCode_F12);
    Bind(project_fkey_command, KeyCode_F13);
    Bind(project_fkey_command, KeyCode_F14);
    Bind(project_fkey_command, KeyCode_F15);
    Bind(project_fkey_command, KeyCode_F16);
    
	// ==== Normal Mode Bindings ====
	SelectMap(mapid_normal);
	ParentMap(mapid_shared);
    
	// Navigation
	Bind(move_left_alpha_numeric_boundary,  KeyCode_H, KeyCode_A);
	Bind(move_right_alpha_numeric_boundary, KeyCode_L, KeyCode_A);
	Bind(move_up_to_blank_line,    KeyCode_K, KeyCode_A);
	Bind(move_down_to_blank_line,  KeyCode_J, KeyCode_A);
    
	Bind(move_line_up,    KeyCode_K, KeyCode_Shift);
	Bind(move_line_down,  KeyCode_J, KeyCode_Shift);
    
	Bind(move_left_alpha_numeric_boundary,  KeyCode_H, KeyCode_S);
	Bind(move_right_alpha_numeric_boundary, KeyCode_L, KeyCode_S);
	Bind(move_up_to_blank_line,    KeyCode_K, KeyCode_S);
	Bind(move_down_to_blank_line,  KeyCode_J, KeyCode_S);
    
	Bind(move_left,         KeyCode_H);
	Bind(move_right,        KeyCode_L);
	Bind(move_up,           KeyCode_K);
	Bind(move_down,         KeyCode_J);
    
	Bind(go_to_insert_mode, KeyCode_I);
	Bind(set_mark,          KeyCode_Space);
	Bind(cursor_mark_swap,  KeyCode_Space, KeyCode_Shift);
	Bind(set_mark,          KeyCode_S);
    
	Bind(insert_at_end, KeyCode_I, KeyCode_Shift);
	Bind(insert_at_beginning, KeyCode_I, KeyCode_Alt);
	Bind(insert_on_bottom, KeyCode_O);
	Bind(insert_on_top, KeyCode_O, KeyCode_Shift);
    
	Bind(goto_line, KeyCode_G);
	Bind(goto_beginning_of_file, KeyCode_G, KeyCode_Shift);
	Bind(goto_end_of_file, KeyCode_G, KeyCode_Control);
    
	// character manipulation
	Bind(delete_char,  KeyCode_C, KeyCode_Shift);
	Bind(cut,          KeyCode_C);
	Bind(delete_line,  KeyCode_D, KeyCode_Shift);
	Bind(delete_range, KeyCode_D);
    
	Bind(select_line,    KeyCode_X);
	Bind(duplicate_line, KeyCode_X, KeyCode_Shift);
	Bind(comment_line_toggle, KeyCode_Z);
    
	Bind(undo, KeyCode_U);
	Bind(redo, KeyCode_U, KeyCode_Shift);
    
	Bind(copy,  KeyCode_Y);
	Bind(paste, KeyCode_P);
    
	Bind(modal_write_todo, KeyCode_T);
	Bind(modal_write_note, KeyCode_T, KeyCode_Shift);
	Bind(modal_write_hack, KeyCode_T, KeyCode_Alt);
    
	Bind(new_line_insert, KeyCode_Return);
    
	// Search and replace
	Bind(search, KeyCode_F);
	Bind(reverse_search, KeyCode_F, KeyCode_Shift);
    
	Bind(query_replace, KeyCode_R);
	Bind(query_replace_selection, KeyCode_R, KeyCode_Shift);
    
	// Open file and panels
	Bind(interactive_open_or_new, KeyCode_N);
	Bind(custom_change_active_panel, KeyCode_W);
	Bind(open_panel_vsplit, KeyCode_W, KeyCode_Shift);
	Bind(open_panel_hsplit, KeyCode_W, KeyCode_Alt);
	Bind(close_panel, KeyCode_W, KeyCode_Control);
	Bind(interactive_kill_buffer, KeyCode_B, KeyCode_Shift);
	Bind(interactive_switch_buffer, KeyCode_B);
	Bind(kill_buffer, KeyCode_B, KeyCode_Control);
    
	// Macros
	Bind(start_macro_recording, KeyCode_Q, KeyCode_Shift);
	Bind(end_macro_recording, KeyCode_Q);
	Bind(keyboard_macro_replay, KeyCode_M);
    
	Bind(command_lister, KeyCode_Semicolon, KeyCode_Shift);
    
	// ==== Insert Mode Bindings ====
	SelectMap(mapid_insert);
	ParentMap(mapid_shared);
	if (global_config.enable_virtual_whitespace == true) {
		BindTextInput(write_text_and_auto_indent);
	} else {
		BindTextInput(write_text_input);
	}
	Bind(word_complete, KeyCode_P, KeyCode_Control);
	Bind(insert_tab, KeyCode_Tab);
    
	// To make sure that the default bindings on the buffers will be mapid_normal
	SelectMap(mapid_file);
	ParentMap(mapid_normal);
    
	SelectMap(mapid_code);
	ParentMap(mapid_file);
}

BUFFER_HOOK_SIG(custom_begin_buffer)
{
    ProfileScope(app, "begin buffer");
    
    Scratch_Block scratch(app);
    
    b32 treat_as_code = deboni_check_language(app, buffer_id);
    
    go_to_normal_mode(app);
    Managed_Scope scope = buffer_get_managed_scope(app, buffer_id);
    //Command_Map_ID *map_id_ptr = scope_attachment(app, scope, buffer_map_id, Command_Map_ID);
    
    Line_Ending_Kind setting = guess_line_ending_kind_from_buffer(app, buffer_id);
    Line_Ending_Kind *eol_setting = scope_attachment(app, scope, buffer_eol_setting, Line_Ending_Kind);
    *eol_setting = setting;
    
    // NOTE(allen): Decide buffer settings
    b32 wrap_lines = true;
    b32 use_lexer = false;
    if (treat_as_code){
        wrap_lines = global_config.enable_code_wrapping;
        use_lexer = true;
    }
    
    String_Const_u8 buffer_name = push_buffer_base_name(app, scratch, buffer_id);
    if (buffer_name.size > 0 && buffer_name.str[0] == '*' && buffer_name.str[buffer_name.size - 1] == '*'){
        wrap_lines = global_config.enable_output_wrapping;
    }
    
    if (use_lexer){
        ProfileBlock(app, "begin buffer kick off lexer");
        Async_Task *lex_task_ptr = scope_attachment(app, scope, buffer_lex_task, Async_Task);
        *lex_task_ptr = async_task_no_dep(&global_async_system, do_full_lex_async, make_data_struct(&buffer_id));
    }
    
    {
        b32 *wrap_lines_ptr = scope_attachment(app, scope, buffer_wrap_lines, b32);
        *wrap_lines_ptr = wrap_lines;
    }
    
    if (use_lexer){
        buffer_set_layout(app, buffer_id, layout_virt_indent_index_generic);
    }
    else{
        if (treat_as_code){
            buffer_set_layout(app, buffer_id, layout_virt_indent_literal_generic);
        }
        else{
            buffer_set_layout(app, buffer_id, layout_generic);
        }
    }
    
    // no meaning for return
    return(0);
}

function void
custom_view_change_buffer(Application_Links *app, View_ID view_id,
                          Buffer_ID old_buffer_id, Buffer_ID new_buffer_id)
{
    Managed_Scope scope = view_get_managed_scope(app, view_id);
    Buffer_ID *prev_buffer_id = scope_attachment(app, scope, view_previous_buffer, Buffer_ID);
	if (prev_buffer_id != 0){
		*prev_buffer_id = old_buffer_id;
	}
    deboni_check_language(app, new_buffer_id);
}

void
custom_layer_init(Application_Links *app)
{
	Thread_Context *tctx = get_thread_context(app);
    
	// Setup for default framework
	default_framework_init(app);
	set_all_default_hooks(app);
    
    // Custom hooks
    set_custom_hook(app, HookID_BeginBuffer, custom_begin_buffer);
    set_custom_hook(app, HookID_RenderCaller, custom_render_caller);
    set_custom_hook(app, HookID_ViewChangeBuffer, custom_view_change_buffer);
    
	setupBindings(tctx, app);
}

#endif
